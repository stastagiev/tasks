﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CardSorter.Entities;
using System.Collections.Generic;
using CardSorter.Helpers;
using System.Linq;

namespace CardSorter.Tests
{
	[TestClass]
	public class CardHelperTests
	{
		[TestMethod]
		public void SortChain_SuccessfullySort()
		{
			var cardChain = new List<Card>()
			{
				new Card { StartPoint = "Мельбурн ", EndPoint = "Кельн" },
				new Card { StartPoint = "Москва", EndPoint = "Париж" },
				new Card { StartPoint = "Кельн", EndPoint = "Москва" }
			};

			var sortedCardChain = CardHelper.SortChain(cardChain).ToList();

			//assert
			Assert.AreEqual(sortedCardChain[0], cardChain[0]);
			Assert.AreEqual(sortedCardChain[1], cardChain[2]);
			Assert.AreEqual(sortedCardChain[2], cardChain[1]);
		}

		[TestMethod]
		[ExpectedException(typeof(ArgumentNullException))]
		public void SortChain_ThrowArgumentNullException()
		{
			List<Card> cardChain = null;

			var sortedCardChain = CardHelper.SortChain(cardChain).ToList();
		}

		[TestMethod]
		public void SortChain_NotExceptptionForOne()
		{
			var cardChain = new List<Card>()
			{
				new Card { StartPoint = "Мельбурн ", EndPoint = "Кельн" }
			};

			var sortedCardChain = CardHelper.SortChain(cardChain).ToList();

			//assert
			Assert.AreEqual(sortedCardChain[0], cardChain[0]);
		}
	}
}
