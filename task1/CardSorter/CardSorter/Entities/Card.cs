﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardSorter.Entities
{
	public class Card
	{
		public string StartPoint { get; set; }
		public string EndPoint { get; set; }
	}
}
