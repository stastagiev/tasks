﻿using CardSorter.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardSorter.Helpers
{
	public class CardHelper
	{
		public static IEnumerable<Card> SortChain(IEnumerable<Card> chain)
		{
			if (chain == null)
				throw new ArgumentNullException("cards");

			var dict = new Dictionary<string, Card>();
			foreach (var card in chain)
			{
				dict.Add(card.EndPoint, null);
			}

			Card firstCard = null;
			foreach (var card in chain)
			{
				if (!dict.ContainsKey(card.StartPoint))
					firstCard = card;
					
				dict[card.StartPoint] = card;
			}
			var currentCard = firstCard;
			while(currentCard != null)
			{
				yield return currentCard;
				currentCard = dict[currentCard.EndPoint];
			}
		}
	}
}
